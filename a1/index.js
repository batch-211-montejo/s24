// console.log("Hello")

 let num = 2;
 let getCube = Math.pow(num,3);
 console.log(`The cube of ${num} is ${getCube}`)

 let address = {
 	houseNumber: "258",
 	street: "Washington Ave NW",
 	city: "California",
 	postal: "90011"
 }
 console.log(`I live at ${address.houseNumber} ${address.street}, ${address.city} ${address.postal}`)

 let animal = {
 	name: "Lolong",
 	typeOfCrocodile: "saltwater crocodile",
 	weight: "1075 kgs",
 	measurement: "20 ft 3 in"
 }
 console.log(`${animal.name} was a ${animal.typeOfCrocodile}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}`)


 const numbers = [1,2,3,4,5]
 	numbers.forEach(function(number){
 		console.log(number)
 	})

 const reduceNumber = (a,b,c,d,e) => {
 	return a+b+c+d+e;
 }
 let total = reduceNumber(1,2,3,4,5)
 console.log(total)


class Dog {
	constructor (name,age,breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog ("Adi", "6 months", "Husky");
console.log(myDog)
